#!/usr/bin/env bash
set -e

# $1 - tenant name
# $2 - version

if [ "$1" == "" ] || [ "$2" == "" ]; then
echo "Provide: tenant name and version."
exit 1
fi

services=(config question-sets change claims complaints crm workflow-engine)

for service in "${services[@]}"; do
bash create_dump.sh $service $1 5432 $2
done

