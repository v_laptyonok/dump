#!/usr/bin/env bash
set -e

# $1 - service name
# $2 - tenant name
# $3 - port
# $4 - version

if [ "$1" == "" ] || [ "$2" == "" ] || [ "$3" == "" ] || [ "$4" == "" ]; then
echo "Provide: service name, tenant name, port and version."
exit 1
fi

PGPASSWORD_save=$PGPASSWORD

export PGPASSWORD="postgres"

export PGPASSWORD="postgres"
pg_dump clm-$1-$2 -U postgres -h 127.0.0.1 -p $3 -Fc > dump-clm-$1-$2-$4;

