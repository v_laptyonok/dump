#!/usr/bin/env bash
set -e

# $1 - tenant name
# $2 - version

if [ "$1" == "" ] || [ "$2" == "" ]; then
echo "Provide: tenant name and version."
exit 1
fi

services=(config question-sets change claims complaints crm workflow-engine)

for service in "${services[@]}"; do
PGPASSWORD="postgres" psql -h 127.0.0.1 -p 5432 -U postgres -c "drop database \"clm-$service-$1\";"
PGPASSWORD="postgres" psql -h 127.0.0.1 -p 5432 -U postgres -c "create database \"clm-$service-$1\";"
PGPASSWORD="postgres" pg_restore -h 127.0.0.1 -p 5432 -U postgres -d clm-$service-$1 dump-clm-$service-$1-$2
done

